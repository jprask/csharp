﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Creational.Assignments.Builder {
    public class MobilePhone {
        private Model Model { get; set; }
        private OperatingSystem Os { get; set; }
        private Screen Screen { get; set; }
        private int Battery { get; set; }
    }
}
