﻿using System;
using System.Windows.Forms;

namespace DatabaseApp {
   public partial class FrmAuthorsSearch : Form {
      private Authors authors = new Authors();
      public FrmAuthorsSearch() {
         InitializeComponent();
      }

      private void btnSearch_Click(object sender, EventArgs e) {
         dgvAuthors.DataSource = authors.Search(tbName.Text);
      }

      private void btnEdit_Click(object sender, EventArgs e) {
         try {
            string id = dgvAuthors.CurrentRow.Cells[0].Value.ToString();
            FrmAuthorsData frm = new FrmAuthorsData(id);
            frm.ShowDialog();
         }
         catch { }
      }

        private void btnInsert_Click(object sender, EventArgs e) {
            FrmAuthorsData frm = new FrmAuthorsData("");
            frm.ShowDialog();
        }
    }
}
