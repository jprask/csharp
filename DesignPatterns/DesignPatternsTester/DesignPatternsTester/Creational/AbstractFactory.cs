﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DesignPatterns.Creational.AbstractFactory;

namespace DesignPatternsTester.Creational {
   public class AbstractFactory {
      public void Run() {
         //original example
         //https://www.codeproject.com/Articles/1252464/Abstract-Factory-Design-Pattern-with-simple-Csharp

         CarClient hondaClient;

         Console.WriteLine("--- HONDA Car Factory ----");
         hondaClient = new CarClient(new HondaFactory(),
                                     Segment.Compact);
         Console.WriteLine("Manufactuering " + hondaClient.Sedan);
         Console.WriteLine("Manufactuering " + hondaClient.Suv);

         hondaClient = new CarClient(new HondaFactory(),
                                     Segment.Full);
         Console.WriteLine("Manufactuering " + hondaClient.Sedan);
         Console.WriteLine("Manufactuering " + hondaClient.Suv);

         Console.WriteLine("");

         CarClient toyotaClient;
         Console.WriteLine("--- TOYOTA Car Factory ----");
         toyotaClient = new CarClient(new ToyotaFactory(),
                                     Segment.Compact);
         Console.WriteLine("Manufactuering " + toyotaClient.Sedan);
         Console.WriteLine("Manufactuering " + toyotaClient.Suv);

         toyotaClient = new CarClient(new ToyotaFactory(),
                                     Segment.Full);
         Console.WriteLine("Manufactuering " + toyotaClient.Sedan);
         Console.WriteLine("Manufactuering " + toyotaClient.Suv);
      }
   }
}
