﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AccessManager {
   public class AccessLog {
      //For more information about List read C# Programming Yellow Book (Rob Milespag) pg 145
      private List<Access> AccessList;

      public AccessLog() {
         AccessList = new List<Access>();
      }

      public void Add(string ip) {
         AccessList.Add(new Access(ip));
         /* you can do it step by step:
         Access access = new Access(ip);
         AccessList.Add(access);
         */
      }

      public List<Access> All() {
         return AccessList;
      }

      //here we have Polymorphism with overload concept
      public List<Access> All(string ip) {
         //TODO return a List with the access
         return null;
      }

      public int Count() {
         return AccessList.Count;
         //transform this method in a readonly proporty 
      }

      public Access FirstAccess() {
         return AccessList[0];
      }

      public Access FirstAccess(string ip) {
         //TODO return the date of the first access
         return null;
      }

      public Access LastAccess() {
         return AccessList[AccessList.Count - 1];
      }

      public Access LastAccess(string ip) {
         //TODO return the date of the last access
         return null;
      }
   }
}
