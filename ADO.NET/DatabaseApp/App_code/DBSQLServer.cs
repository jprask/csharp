﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace SQLServer {
    public static class DB {
        private static string dataSource = "localhost";
        private static string database = "pubs";
        private static string user = "gest";
        private static string password = "gest@2018";
        private static SqlConnection connection;
        public static SqlConnection Connection { get { return Open(); } }
        //instancia e abre a conexao
        public static SqlConnection Open() {
            connection = new SqlConnection();
            string cs = "Data Source={0};Initial Catalog={1};User id={2};Password={3};";
            connection.ConnectionString = string.Format(cs, dataSource, database, user, password);
            try {
                connection.Open();
                return connection;
            }
            catch (Exception error) {
                throw error;
            }

        }//public static SqlConnection Open()

        //close connection
        public static void Close() {
            connection.Close();
        }
    }//public static class SQLServer 

    public static class SQL {
        public static DataTable Select(string sql) {
            SqlCommand cmd = new SqlCommand(sql, DB.Connection);
            return Select(cmd);
        }//public static DataTable Select(string sql)

        public static DataTable Select(SqlCommand cmd) {
            cmd.Connection = DB.Connection;
            SqlDataAdapter da = new SqlDataAdapter(cmd);

            //DataTable store the dataset resulting from SELECT (bidirectional)
            DataTable dt = new DataTable();
            try {
                //fill DataTable 
                da.Fill(dt);
            }
            catch (Exception error) {
                throw error;
            }
            finally {
                DB.Close();
            }
            return dt;
        }//public static DataTable Select(SqlCommand cmd)

        public static SqlDataReader ExecuteReader(string sql) {
            SqlCommand cmd = new SqlCommand(sql, DB.Connection);
            return ExecuteReader(cmd);
        }//public static SqlDataReader ExecuteReader(string sql) 

        public static SqlDataReader ExecuteReader(SqlCommand cmd) {
            cmd.Connection = DB.Connection;
            SqlDataReader rd;
            try {
                rd = cmd.ExecuteReader();
            }
            catch (Exception error) {
                throw error;
            }
            return rd;
        }//public static SqlDataReader ExecuteReader(SqlCommand cmd) 

        public static Object ExecuteScalar(string sql) {
            SqlCommand cmd = new SqlCommand(sql, DB.Connection);
            return ExecuteScalar(cmd);
        }//public static Object ExecuteScalar(string sql)

        public static Object ExecuteScalar(SqlCommand cmd) {
            cmd.Connection = DB.Connection;
            Object obj;
            try {
                obj = cmd.ExecuteScalar();
                DB.Close();
            }
            catch (Exception error) {
                throw error;
            }
            finally {
                DB.Close();
            }
            return obj;
        }//public static Object ExecuteScalar(SqlCommand cmd)

        public static int ExecuteNonQuery(string sql) {
            SqlCommand cmd = new SqlCommand(sql, DB.Connection);
            return ExecuteNonQuery(cmd);
        }//public static int ExecuteNonQuery(string sql)

        public static int ExecuteNonQuery(SqlCommand cmd) {
            //ExecuteNonQuery returns number of rows affected (ex: 2 rows updated)
            cmd.Connection = DB.Connection;
            int i;
            try {
                i = cmd.ExecuteNonQuery();
            }
            catch (Exception error) {
                throw error;
            }
            finally {
                DB.Close();
            }
            return i;
        }//public static int ExecuteNonQuery(SqlCommand cmd)

    } //public static class SQL
}//namespace SQLServer
