﻿namespace DatabaseApp
{
    partial class FrmADOObjects
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
         this.btnConnection = new System.Windows.Forms.Button();
         this.lblConnect = new System.Windows.Forms.Label();
         this.btnCommandExecuteScalar = new System.Windows.Forms.Button();
         this.label1 = new System.Windows.Forms.Label();
         this.lblNumerOfRows = new System.Windows.Forms.Label();
         this.btnCommandExecuteReader = new System.Windows.Forms.Button();
         this.lbAuthors = new System.Windows.Forms.ListBox();
         this.label2 = new System.Windows.Forms.Label();
         this.btnCommandExecuteNonQuery = new System.Windows.Forms.Button();
         this.label3 = new System.Windows.Forms.Label();
         this.label4 = new System.Windows.Forms.Label();
         this.tbStateFrom = new System.Windows.Forms.TextBox();
         this.tbStateTo = new System.Windows.Forms.TextBox();
         this.lblNumerOfRowsUpdated = new System.Windows.Forms.Label();
         this.label6 = new System.Windows.Forms.Label();
         this.label5 = new System.Windows.Forms.Label();
         this.label7 = new System.Windows.Forms.Label();
         this.btnCommandExecuteXmlReader = new System.Windows.Forms.Button();
         this.tbXML = new System.Windows.Forms.TextBox();
         this.btnSqlDataAdapter = new System.Windows.Forms.Button();
         this.label8 = new System.Windows.Forms.Label();
         this.tbName = new System.Windows.Forms.TextBox();
         this.dtGrdAuthors = new System.Windows.Forms.DataGridView();
         this.label9 = new System.Windows.Forms.Label();
         this.label10 = new System.Windows.Forms.Label();
         this.label11 = new System.Windows.Forms.Label();
         ((System.ComponentModel.ISupportInitialize)(this.dtGrdAuthors)).BeginInit();
         this.SuspendLayout();
         // 
         // btnConnection
         // 
         this.btnConnection.Location = new System.Drawing.Point(16, 15);
         this.btnConnection.Margin = new System.Windows.Forms.Padding(4);
         this.btnConnection.Name = "btnConnection";
         this.btnConnection.Size = new System.Drawing.Size(119, 41);
         this.btnConnection.TabIndex = 0;
         this.btnConnection.Text = "Connect";
         this.btnConnection.UseVisualStyleBackColor = true;
         this.btnConnection.Click += new System.EventHandler(this.btnConnection_Click);
         // 
         // lblConnect
         // 
         this.lblConnect.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
         this.lblConnect.Location = new System.Drawing.Point(168, 8);
         this.lblConnect.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.lblConnect.Name = "lblConnect";
         this.lblConnect.Size = new System.Drawing.Size(715, 63);
         this.lblConnect.TabIndex = 1;
         this.lblConnect.Text = "Connection Result";
         // 
         // btnCommandExecuteScalar
         // 
         this.btnCommandExecuteScalar.Location = new System.Drawing.Point(13, 91);
         this.btnCommandExecuteScalar.Margin = new System.Windows.Forms.Padding(4);
         this.btnCommandExecuteScalar.Name = "btnCommandExecuteScalar";
         this.btnCommandExecuteScalar.Size = new System.Drawing.Size(225, 39);
         this.btnCommandExecuteScalar.TabIndex = 2;
         this.btnCommandExecuteScalar.Text = "Command - ExecuteScalar()";
         this.btnCommandExecuteScalar.UseVisualStyleBackColor = true;
         this.btnCommandExecuteScalar.Click += new System.EventHandler(this.btnCommandExecuteScalar_Click);
         // 
         // label1
         // 
         this.label1.AutoSize = true;
         this.label1.Location = new System.Drawing.Point(278, 104);
         this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label1.Name = "label1";
         this.label1.Size = new System.Drawing.Size(207, 16);
         this.label1.TabIndex = 3;
         this.label1.Text = "Table Authors - Number of Rows: ";
         // 
         // lblNumerOfRows
         // 
         this.lblNumerOfRows.AutoSize = true;
         this.lblNumerOfRows.Location = new System.Drawing.Point(482, 104);
         this.lblNumerOfRows.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.lblNumerOfRows.Name = "lblNumerOfRows";
         this.lblNumerOfRows.Size = new System.Drawing.Size(22, 16);
         this.lblNumerOfRows.TabIndex = 4;
         this.lblNumerOfRows.Text = "00";
         // 
         // btnCommandExecuteReader
         // 
         this.btnCommandExecuteReader.Location = new System.Drawing.Point(12, 202);
         this.btnCommandExecuteReader.Margin = new System.Windows.Forms.Padding(4);
         this.btnCommandExecuteReader.Name = "btnCommandExecuteReader";
         this.btnCommandExecuteReader.Size = new System.Drawing.Size(225, 36);
         this.btnCommandExecuteReader.TabIndex = 20;
         this.btnCommandExecuteReader.Text = "Command - ExecuteReader()";
         this.btnCommandExecuteReader.UseVisualStyleBackColor = true;
         this.btnCommandExecuteReader.Click += new System.EventHandler(this.btnCommandExecuteReader_Click);
         // 
         // lbAuthors
         // 
         this.lbAuthors.FormattingEnabled = true;
         this.lbAuthors.ItemHeight = 16;
         this.lbAuthors.Location = new System.Drawing.Point(267, 202);
         this.lbAuthors.Margin = new System.Windows.Forms.Padding(4);
         this.lbAuthors.Name = "lbAuthors";
         this.lbAuthors.ScrollAlwaysVisible = true;
         this.lbAuthors.Size = new System.Drawing.Size(237, 100);
         this.lbAuthors.TabIndex = 21;
         // 
         // label2
         // 
         this.label2.AutoSize = true;
         this.label2.Location = new System.Drawing.Point(12, 251);
         this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label2.Name = "label2";
         this.label2.Size = new System.Drawing.Size(239, 16);
         this.label2.TabIndex = 7;
         this.label2.Text = "Get Authors List and show it in a ListBox";
         // 
         // btnCommandExecuteNonQuery
         // 
         this.btnCommandExecuteNonQuery.Location = new System.Drawing.Point(13, 147);
         this.btnCommandExecuteNonQuery.Margin = new System.Windows.Forms.Padding(4);
         this.btnCommandExecuteNonQuery.Name = "btnCommandExecuteNonQuery";
         this.btnCommandExecuteNonQuery.Size = new System.Drawing.Size(225, 36);
         this.btnCommandExecuteNonQuery.TabIndex = 10;
         this.btnCommandExecuteNonQuery.Text = "Command - ExecuteNonQuery()";
         this.btnCommandExecuteNonQuery.UseVisualStyleBackColor = true;
         this.btnCommandExecuteNonQuery.Click += new System.EventHandler(this.btnCommandExecuteNonQuery_Click);
         // 
         // label3
         // 
         this.label3.AutoSize = true;
         this.label3.Location = new System.Drawing.Point(279, 157);
         this.label3.Name = "label3";
         this.label3.Size = new System.Drawing.Size(165, 16);
         this.label3.TabIndex = 12;
         this.label3.Text = "Change Authos State from ";
         // 
         // label4
         // 
         this.label4.AutoSize = true;
         this.label4.Location = new System.Drawing.Point(532, 157);
         this.label4.Name = "label4";
         this.label4.Size = new System.Drawing.Size(25, 16);
         this.label4.TabIndex = 22;
         this.label4.Text = "To";
         // 
         // tbStateFrom
         // 
         this.tbStateFrom.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
         this.tbStateFrom.Location = new System.Drawing.Point(450, 154);
         this.tbStateFrom.Name = "tbStateFrom";
         this.tbStateFrom.Size = new System.Drawing.Size(76, 22);
         this.tbStateFrom.TabIndex = 23;
         // 
         // tbStateTo
         // 
         this.tbStateTo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
         this.tbStateTo.Location = new System.Drawing.Point(563, 154);
         this.tbStateTo.Name = "tbStateTo";
         this.tbStateTo.Size = new System.Drawing.Size(76, 22);
         this.tbStateTo.TabIndex = 24;
         // 
         // lblNumerOfRowsUpdated
         // 
         this.lblNumerOfRowsUpdated.AutoSize = true;
         this.lblNumerOfRowsUpdated.Location = new System.Drawing.Point(828, 160);
         this.lblNumerOfRowsUpdated.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.lblNumerOfRowsUpdated.Name = "lblNumerOfRowsUpdated";
         this.lblNumerOfRowsUpdated.Size = new System.Drawing.Size(22, 16);
         this.lblNumerOfRowsUpdated.TabIndex = 26;
         this.lblNumerOfRowsUpdated.Text = "00";
         // 
         // label6
         // 
         this.label6.AutoSize = true;
         this.label6.Location = new System.Drawing.Point(663, 160);
         this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
         this.label6.Name = "label6";
         this.label6.Size = new System.Drawing.Size(157, 16);
         this.label6.TabIndex = 25;
         this.label6.Text = "Number of rows updated:";
         // 
         // label5
         // 
         this.label5.AutoSize = true;
         this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label5.Location = new System.Drawing.Point(14, 479);
         this.label5.Name = "label5";
         this.label5.Size = new System.Drawing.Size(68, 16);
         this.label5.TabIndex = 27;
         this.label5.Text = "Exercise";
         // 
         // label7
         // 
         this.label7.AutoSize = true;
         this.label7.Location = new System.Drawing.Point(16, 503);
         this.label7.Name = "label7";
         this.label7.Size = new System.Drawing.Size(564, 16);
         this.label7.TabIndex = 28;
         this.label7.Text = "1. Change the TextBox \"tbStateFrom\" to a ComboBox with the list of states in the " +
    "table \"authors\"";
         // 
         // btnCommandExecuteXmlReader
         // 
         this.btnCommandExecuteXmlReader.Location = new System.Drawing.Point(15, 311);
         this.btnCommandExecuteXmlReader.Margin = new System.Windows.Forms.Padding(4);
         this.btnCommandExecuteXmlReader.Name = "btnCommandExecuteXmlReader";
         this.btnCommandExecuteXmlReader.Size = new System.Drawing.Size(225, 36);
         this.btnCommandExecuteXmlReader.TabIndex = 29;
         this.btnCommandExecuteXmlReader.Text = "Command - ExecuteXmlReader()";
         this.btnCommandExecuteXmlReader.UseVisualStyleBackColor = true;
         this.btnCommandExecuteXmlReader.Click += new System.EventHandler(this.btnCommandExecuteXmlReader_Click);
         // 
         // tbXML
         // 
         this.tbXML.Location = new System.Drawing.Point(12, 354);
         this.tbXML.Multiline = true;
         this.tbXML.Name = "tbXML";
         this.tbXML.Size = new System.Drawing.Size(492, 108);
         this.tbXML.TabIndex = 30;
         // 
         // btnSqlDataAdapter
         // 
         this.btnSqlDataAdapter.Location = new System.Drawing.Point(535, 202);
         this.btnSqlDataAdapter.Margin = new System.Windows.Forms.Padding(4);
         this.btnSqlDataAdapter.Name = "btnSqlDataAdapter";
         this.btnSqlDataAdapter.Size = new System.Drawing.Size(138, 36);
         this.btnSqlDataAdapter.TabIndex = 31;
         this.btnSqlDataAdapter.Text = "SqlDataAdapter";
         this.btnSqlDataAdapter.UseVisualStyleBackColor = true;
         this.btnSqlDataAdapter.Click += new System.EventHandler(this.btnSqlDataAdapter_Click);
         // 
         // label8
         // 
         this.label8.AutoSize = true;
         this.label8.Location = new System.Drawing.Point(699, 212);
         this.label8.Name = "label8";
         this.label8.Size = new System.Drawing.Size(45, 16);
         this.label8.TabIndex = 32;
         this.label8.Text = "Name";
         // 
         // tbName
         // 
         this.tbName.Location = new System.Drawing.Point(750, 209);
         this.tbName.Name = "tbName";
         this.tbName.Size = new System.Drawing.Size(100, 22);
         this.tbName.TabIndex = 33;
         // 
         // dtGrdAuthors
         // 
         this.dtGrdAuthors.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
         this.dtGrdAuthors.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
         this.dtGrdAuthors.Location = new System.Drawing.Point(535, 251);
         this.dtGrdAuthors.Name = "dtGrdAuthors";
         this.dtGrdAuthors.Size = new System.Drawing.Size(355, 211);
         this.dtGrdAuthors.TabIndex = 34;
         // 
         // label9
         // 
         this.label9.AutoSize = true;
         this.label9.Location = new System.Drawing.Point(16, 551);
         this.label9.Name = "label9";
         this.label9.Size = new System.Drawing.Size(530, 16);
         this.label9.TabIndex = 35;
         this.label9.Text = "3. Change the \"sql\" variable for a StringBuilder object in the Authors.GetAuthors" +
    " () method";
         // 
         // label10
         // 
         this.label10.AutoSize = true;
         this.label10.Location = new System.Drawing.Point(16, 526);
         this.label10.Name = "label10";
         this.label10.Size = new System.Drawing.Size(474, 16);
         this.label10.TabIndex = 36;
         this.label10.Text = "2. Configure the DataGrid: do not allow editing of records and add the field \"cit" +
    "y\"";
         // 
         // label11
         // 
         this.label11.AutoSize = true;
         this.label11.Location = new System.Drawing.Point(16, 573);
         this.label11.Name = "label11";
         this.label11.Size = new System.Drawing.Size(370, 16);
         this.label11.TabIndex = 37;
         this.label11.Text = "4. Specify a root element in XML and save it to your hard drive";
         // 
         // FrmADOObjects
         // 
         this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.ClientSize = new System.Drawing.Size(901, 597);
         this.Controls.Add(this.label11);
         this.Controls.Add(this.label10);
         this.Controls.Add(this.label9);
         this.Controls.Add(this.dtGrdAuthors);
         this.Controls.Add(this.tbName);
         this.Controls.Add(this.label8);
         this.Controls.Add(this.btnSqlDataAdapter);
         this.Controls.Add(this.tbXML);
         this.Controls.Add(this.btnCommandExecuteXmlReader);
         this.Controls.Add(this.label7);
         this.Controls.Add(this.label5);
         this.Controls.Add(this.lblNumerOfRowsUpdated);
         this.Controls.Add(this.label6);
         this.Controls.Add(this.tbStateTo);
         this.Controls.Add(this.tbStateFrom);
         this.Controls.Add(this.label4);
         this.Controls.Add(this.label3);
         this.Controls.Add(this.btnCommandExecuteNonQuery);
         this.Controls.Add(this.label2);
         this.Controls.Add(this.lbAuthors);
         this.Controls.Add(this.btnCommandExecuteReader);
         this.Controls.Add(this.lblNumerOfRows);
         this.Controls.Add(this.label1);
         this.Controls.Add(this.btnCommandExecuteScalar);
         this.Controls.Add(this.lblConnect);
         this.Controls.Add(this.btnConnection);
         this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.Margin = new System.Windows.Forms.Padding(4);
         this.Name = "FrmADOObjects";
         this.Text = "ADO Objects";
         ((System.ComponentModel.ISupportInitialize)(this.dtGrdAuthors)).EndInit();
         this.ResumeLayout(false);
         this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnConnection;
        private System.Windows.Forms.Label lblConnect;
        private System.Windows.Forms.Button btnCommandExecuteScalar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblNumerOfRows;
        private System.Windows.Forms.Button btnCommandExecuteReader;
        private System.Windows.Forms.ListBox lbAuthors;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnCommandExecuteNonQuery;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbStateFrom;
        private System.Windows.Forms.TextBox tbStateTo;
        private System.Windows.Forms.Label lblNumerOfRowsUpdated;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
      private System.Windows.Forms.Button btnCommandExecuteXmlReader;
      private System.Windows.Forms.TextBox tbXML;
      private System.Windows.Forms.Button btnSqlDataAdapter;
      private System.Windows.Forms.Label label8;
      private System.Windows.Forms.TextBox tbName;
      private System.Windows.Forms.DataGridView dtGrdAuthors;
      private System.Windows.Forms.Label label9;
      private System.Windows.Forms.Label label10;
      private System.Windows.Forms.Label label11;
   }
}