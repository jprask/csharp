﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace DatabaseApp {
    public class Person {
      public string Code { get; set; }
      public string Name { get; set; }
      public string Phone { get; set; }
   }//public class Person

   public class People {
      public List<Person> List = new List<Person>();
      private string xmlFile;
      private XElement xml;

      public People(string xmlFile) {
         this.xmlFile = xmlFile;         
      }

      public void Load() {
         List.Clear();
         this.xml = XElement.Load(xmlFile);
         foreach (XElement x in xml.Elements()) {
            Person p = new Person() {
               Code = x.Attribute("code").Value,
               Name = x.Attribute("name").Value,
               Phone = x.Attribute("phone").Value
            };
            List.Add(p);
         }
      }//public void Load() 

      public void Insert(Person p) {
         XElement x = new XElement("person");
         x.Add(new XAttribute("code", p.Code.ToString()));
         x.Add(new XAttribute("name", p.Name));
         x.Add(new XAttribute("phone", p.Phone));

         xml.Add(x);
         xml.Save(xmlFile);
      }//public void Insert(Person p)

      public void Delete(string code) {
         XElement x = xml.Elements().Where(p => p.Attribute("code").Value.Equals(code.ToString())).First();
         if (x != null) {
            x.Remove();
         }
         xml.Save(xmlFile);
      }//public void Delete(string code)

      public void Edit(Person Person) {
         XElement x = xml.Elements().Where(p => p.Attribute("code").Value.Equals(Person.Code.ToString())).First();
         if (x != null) {
            x.Attribute("name").SetValue(Person.Name);
            x.Attribute("phone").SetValue(Person.Phone);
         }
         xml.Save(xmlFile);
      }//public void Edit(Person Person)

   }//public class People
}//public class Person
