﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DesignPatterns.Creational.Builder;

namespace DesignPatternsTester.Creational {
   public class Builder {
      //original example
      //https://www.dotnettricks.com/learn/designpatterns/builder-design-pattern-dotnet

      public void ShowInfo(Vehicle vehicle) {
         Console.WriteLine("Model: {0}", vehicle.Model);
         Console.WriteLine("Engine: {0}", vehicle.Engine);
         Console.WriteLine("Body: {0}", vehicle.Body);
         Console.WriteLine("Transmission: {0}", vehicle.Transmission);
         string accessories = "";
         foreach (var accessory in vehicle.Accessories) {
            accessories += accessory + ". ";
         }
         Console.WriteLine("Accessories: {0}", accessories);
      }

      public void Run() {
         VehicleCreator vehicleCreator = new VehicleCreator(new HeroBuilder());

         vehicleCreator.CreateVehicle();
         var vehicle = vehicleCreator.GetVehicle();

         ShowInfo(vehicle);

         Console.WriteLine("");

         vehicleCreator = new VehicleCreator(new HondaBuilder());
         vehicleCreator.CreateVehicle();
         vehicle = vehicleCreator.GetVehicle();
         ShowInfo(vehicle);
      }
   }
}
