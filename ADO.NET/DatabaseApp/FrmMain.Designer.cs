﻿namespace DatabaseApp
{
    partial class FrmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
         System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
         this.mnStrpMain = new System.Windows.Forms.MenuStrip();
         this.mainToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
         this.mnItmADOObjects = new System.Windows.Forms.ToolStripMenuItem();
         this.mnItmAuthors = new System.Windows.Forms.ToolStripMenuItem();
         this.mnItmXML = new System.Windows.Forms.ToolStripMenuItem();
         this.tlStrpMain = new System.Windows.Forms.ToolStrip();
         this.toolBtnADOObjects = new System.Windows.Forms.ToolStripButton();
         this.toolBtnAuthors = new System.Windows.Forms.ToolStripButton();
         this.toolBtnXML = new System.Windows.Forms.ToolStripButton();
         this.label1 = new System.Windows.Forms.Label();
         this.lnkLblDatabase = new System.Windows.Forms.LinkLabel();
         this.lnkLblGit = new System.Windows.Forms.LinkLabel();
         this.label2 = new System.Windows.Forms.Label();
         this.linkLabel1 = new System.Windows.Forms.LinkLabel();
         this.label3 = new System.Windows.Forms.Label();
         this.toolBtnXMLFromUrl = new System.Windows.Forms.ToolStripButton();
         this.mnItmXMLFromURL = new System.Windows.Forms.ToolStripMenuItem();
         this.mnStrpMain.SuspendLayout();
         this.tlStrpMain.SuspendLayout();
         this.SuspendLayout();
         // 
         // mnStrpMain
         // 
         this.mnStrpMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mainToolStripMenuItem});
         this.mnStrpMain.Location = new System.Drawing.Point(0, 0);
         this.mnStrpMain.Name = "mnStrpMain";
         this.mnStrpMain.Padding = new System.Windows.Forms.Padding(8, 2, 0, 2);
         this.mnStrpMain.Size = new System.Drawing.Size(656, 24);
         this.mnStrpMain.TabIndex = 1;
         this.mnStrpMain.Text = "menuStrip1";
         // 
         // mainToolStripMenuItem
         // 
         this.mainToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnItmADOObjects,
            this.mnItmAuthors,
            this.mnItmXML,
            this.mnItmXMLFromURL});
         this.mainToolStripMenuItem.Name = "mainToolStripMenuItem";
         this.mainToolStripMenuItem.Size = new System.Drawing.Size(46, 20);
         this.mainToolStripMenuItem.Text = "Main";
         // 
         // mnItmADOObjects
         // 
         this.mnItmADOObjects.Name = "mnItmADOObjects";
         this.mnItmADOObjects.Size = new System.Drawing.Size(152, 22);
         this.mnItmADOObjects.Text = "ADO Objects";
         this.mnItmADOObjects.Click += new System.EventHandler(this.mnItmADOObjects_Click);
         // 
         // mnItmAuthors
         // 
         this.mnItmAuthors.Name = "mnItmAuthors";
         this.mnItmAuthors.Size = new System.Drawing.Size(152, 22);
         this.mnItmAuthors.Text = "Authors";
         this.mnItmAuthors.Click += new System.EventHandler(this.mnItmAuthors_Click);
         // 
         // mnItmXML
         // 
         this.mnItmXML.Name = "mnItmXML";
         this.mnItmXML.Size = new System.Drawing.Size(152, 22);
         this.mnItmXML.Text = "XML";
         this.mnItmXML.Click += new System.EventHandler(this.mnItmXML_Click);
         // 
         // tlStrpMain
         // 
         this.tlStrpMain.ImageScalingSize = new System.Drawing.Size(48, 48);
         this.tlStrpMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolBtnADOObjects,
            this.toolBtnAuthors,
            this.toolBtnXML,
            this.toolBtnXMLFromUrl});
         this.tlStrpMain.Location = new System.Drawing.Point(0, 24);
         this.tlStrpMain.Name = "tlStrpMain";
         this.tlStrpMain.Size = new System.Drawing.Size(656, 55);
         this.tlStrpMain.TabIndex = 2;
         this.tlStrpMain.Text = "toolStrip1";
         // 
         // toolBtnADOObjects
         // 
         this.toolBtnADOObjects.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
         this.toolBtnADOObjects.Image = ((System.Drawing.Image)(resources.GetObject("toolBtnADOObjects.Image")));
         this.toolBtnADOObjects.ImageTransparentColor = System.Drawing.Color.Magenta;
         this.toolBtnADOObjects.Name = "toolBtnADOObjects";
         this.toolBtnADOObjects.Size = new System.Drawing.Size(52, 52);
         this.toolBtnADOObjects.Text = "ADOObjects";
         this.toolBtnADOObjects.Click += new System.EventHandler(this.mnItmADOObjects_Click);
         // 
         // toolBtnAuthors
         // 
         this.toolBtnAuthors.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
         this.toolBtnAuthors.Image = ((System.Drawing.Image)(resources.GetObject("toolBtnAuthors.Image")));
         this.toolBtnAuthors.ImageTransparentColor = System.Drawing.Color.Magenta;
         this.toolBtnAuthors.Name = "toolBtnAuthors";
         this.toolBtnAuthors.Size = new System.Drawing.Size(52, 52);
         this.toolBtnAuthors.Text = "Authors";
         this.toolBtnAuthors.Click += new System.EventHandler(this.mnItmAuthors_Click);
         // 
         // toolBtnXML
         // 
         this.toolBtnXML.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
         this.toolBtnXML.Image = ((System.Drawing.Image)(resources.GetObject("toolBtnXML.Image")));
         this.toolBtnXML.ImageTransparentColor = System.Drawing.Color.Magenta;
         this.toolBtnXML.Name = "toolBtnXML";
         this.toolBtnXML.Size = new System.Drawing.Size(52, 52);
         this.toolBtnXML.Text = "toolStripButton1";
         this.toolBtnXML.Click += new System.EventHandler(this.mnItmXML_Click);
         // 
         // label1
         // 
         this.label1.AutoSize = true;
         this.label1.Location = new System.Drawing.Point(21, 353);
         this.label1.Name = "label1";
         this.label1.Size = new System.Drawing.Size(360, 16);
         this.label1.TabIndex = 3;
         this.label1.Text = "This project uses PUBS Sample Databases for SQL Server";
         // 
         // lnkLblDatabase
         // 
         this.lnkLblDatabase.AutoSize = true;
         this.lnkLblDatabase.Location = new System.Drawing.Point(21, 377);
         this.lnkLblDatabase.Name = "lnkLblDatabase";
         this.lnkLblDatabase.Size = new System.Drawing.Size(396, 16);
         this.lnkLblDatabase.TabIndex = 4;
         this.lnkLblDatabase.TabStop = true;
         this.lnkLblDatabase.Text = "https://www.microsoft.com/en-us/download/details.aspx?id=23654";
         this.lnkLblDatabase.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkLblDatabase_LinkClicked);
         // 
         // lnkLblGit
         // 
         this.lnkLblGit.AutoSize = true;
         this.lnkLblGit.Location = new System.Drawing.Point(21, 443);
         this.lnkLblGit.Name = "lnkLblGit";
         this.lnkLblGit.Size = new System.Drawing.Size(248, 16);
         this.lnkLblGit.TabIndex = 6;
         this.lnkLblGit.TabStop = true;
         this.lnkLblGit.Text = "https://gitlab.com/fernando.prass/csharp";
         this.lnkLblGit.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkLblDatabase_LinkClicked);
         // 
         // label2
         // 
         this.label2.AutoSize = true;
         this.label2.Location = new System.Drawing.Point(21, 425);
         this.label2.Name = "label2";
         this.label2.Size = new System.Drawing.Size(69, 16);
         this.label2.TabIndex = 5;
         this.label2.Text = "Git Project";
         // 
         // linkLabel1
         // 
         this.linkLabel1.AutoSize = true;
         this.linkLabel1.Location = new System.Drawing.Point(399, 443);
         this.linkLabel1.Name = "linkLabel1";
         this.linkLabel1.Size = new System.Drawing.Size(214, 16);
         this.linkLabel1.TabIndex = 8;
         this.linkLabel1.TabStop = true;
         this.linkLabel1.Text = "https://twitter.com/oFernandoPrass";
         this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkLblDatabase_LinkClicked);
         // 
         // label3
         // 
         this.label3.AutoSize = true;
         this.label3.Location = new System.Drawing.Point(399, 425);
         this.label3.Name = "label3";
         this.label3.Size = new System.Drawing.Size(123, 16);
         this.label3.TabIndex = 7;
         this.label3.Text = "By Fernando Prass";
         // 
         // toolBtnXMLFromUrl
         // 
         this.toolBtnXMLFromUrl.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
         this.toolBtnXMLFromUrl.Image = ((System.Drawing.Image)(resources.GetObject("toolBtnXMLFromUrl.Image")));
         this.toolBtnXMLFromUrl.ImageTransparentColor = System.Drawing.Color.Magenta;
         this.toolBtnXMLFromUrl.Name = "toolBtnXMLFromUrl";
         this.toolBtnXMLFromUrl.Size = new System.Drawing.Size(52, 52);
         this.toolBtnXMLFromUrl.Text = "toolStripButton1";
         this.toolBtnXMLFromUrl.Click += new System.EventHandler(this.mnItmXMLFromURL_Click);
         // 
         // mnItmXMLFromURL
         // 
         this.mnItmXMLFromURL.Name = "mnItmXMLFromURL";
         this.mnItmXMLFromURL.Size = new System.Drawing.Size(152, 22);
         this.mnItmXMLFromURL.Text = "XML from URL";
         this.mnItmXMLFromURL.Click += new System.EventHandler(this.mnItmXMLFromURL_Click);
         // 
         // FrmMain
         // 
         this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.ClientSize = new System.Drawing.Size(656, 486);
         this.Controls.Add(this.linkLabel1);
         this.Controls.Add(this.label3);
         this.Controls.Add(this.lnkLblGit);
         this.Controls.Add(this.label2);
         this.Controls.Add(this.lnkLblDatabase);
         this.Controls.Add(this.label1);
         this.Controls.Add(this.tlStrpMain);
         this.Controls.Add(this.mnStrpMain);
         this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.MainMenuStrip = this.mnStrpMain;
         this.Margin = new System.Windows.Forms.Padding(4);
         this.Name = "FrmMain";
         this.Text = "Main form";
         this.mnStrpMain.ResumeLayout(false);
         this.mnStrpMain.PerformLayout();
         this.tlStrpMain.ResumeLayout(false);
         this.tlStrpMain.PerformLayout();
         this.ResumeLayout(false);
         this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip mnStrpMain;
        private System.Windows.Forms.ToolStripMenuItem mainToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mnItmADOObjects;
        private System.Windows.Forms.ToolStrip tlStrpMain;
        private System.Windows.Forms.ToolStripButton toolBtnADOObjects;
      private System.Windows.Forms.Label label1;
      private System.Windows.Forms.LinkLabel lnkLblDatabase;
      private System.Windows.Forms.LinkLabel lnkLblGit;
      private System.Windows.Forms.Label label2;
      private System.Windows.Forms.LinkLabel linkLabel1;
      private System.Windows.Forms.Label label3;
      private System.Windows.Forms.ToolStripButton toolBtnAuthors;
      private System.Windows.Forms.ToolStripMenuItem mnItmAuthors;
      private System.Windows.Forms.ToolStripMenuItem mnItmXML;
      private System.Windows.Forms.ToolStripButton toolBtnXML;
      private System.Windows.Forms.ToolStripMenuItem mnItmXMLFromURL;
      private System.Windows.Forms.ToolStripButton toolBtnXMLFromUrl;
   }
}

