﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccessManager {
   public class IP {
      private string number;
      private Enumerator.IpType ipType;
      public string Number {
         get { return number; }
      }
      public Enumerator.IpType Type {
         get { return ipType; }
         private set {
            //TODO write here code to identify the IP type
            //sugestion: use regular expression
         }
      }   

      public IP(string number) {
         this.number = number;
      }
   }
}
