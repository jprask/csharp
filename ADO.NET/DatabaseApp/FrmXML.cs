﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DatabaseApp {
   public partial class FrmXML : Form {
      People people = new People("People.xml");

      public FrmXML() {
         InitializeComponent();
      }

      private void btnInsert_Click(object sender, EventArgs e) {
         Person p = new Person() {
            Code = tbCode.Text,
            Name = tbName.Text,
            Phone = tbPhone.Text
         };
         people.Insert(p);
         LoadPeople();

      }

      private void FrmXML_Load(object sender, EventArgs e) {
         LoadPeople();
      }

      private void LoadPeople() {
         people.Load();
         dgvPeople.DataSource = people.List;
         dgvPeople.Refresh();
      }

      private void dgvPeople_SelectionChanged(object sender, EventArgs e) {
         if (dgvPeople.SelectedRows.Count > 0) {
            int index = dgvPeople.SelectedRows[0].Index;
            if (index >= 0) {
               tbCode.Text = people.List[index].Code.ToString();
               tbName.Text = people.List[index].Name;
               tbPhone.Text = people.List[index].Phone;
            }
         }
      }

      private void btnEdit_Click(object sender, EventArgs e) {
         Person p = new Person() {
            Code = tbCode.Text,
            Name = tbName.Text,
            Phone = tbPhone.Text
         };
         people.Edit(p);
         LoadPeople();
      }

      private void btnDelete_Click(object sender, EventArgs e) {
         if (dgvPeople.SelectedRows.Count > 0) {
            int index = dgvPeople.SelectedRows[0].Index;
            people.Delete(people.List[index].Code);
            LoadPeople();
         }
      }

      private void openFileDialog1_FileOk(object sender, CancelEventArgs e) {

      }
   }
}
