﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Creational.Assignments.Builder {
    public class AppleBuilder : PhoneBuilder {
        private MobilePhone Phone;

        private void SetModel() {

        }

        private void buildPhone() {
            this.Phone = new MobilePhone();
            this.SetModel();
            this.SetOS();
            this.SetScreen();
            this.SetBattery();
        }

        public MobilePhone getPhone() {
            throw new NotImplementedException();
        }
    }
}
