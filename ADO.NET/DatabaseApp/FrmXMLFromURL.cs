﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DatabaseApp {
   public partial class FrmXMLFromURL : Form {
      private XMLFromURL xml = new XMLFromURL();
      public FrmXMLFromURL() {
         InitializeComponent();
      }

      private void btnLoad_Click(object sender, EventArgs e) {
         xml.LoadCities();
         dgvCities.DataSource = xml.Cities;
      }

      private void dgvCities_SelectionChanged(object sender, EventArgs e) {
         if (dgvCities.SelectedRows.Count > 0) {
            string code = xml.Cities[dgvCities.SelectedRows[0].Index].Code;
            dgvxmlWeatherForecast.DataSource = xml.WeatherForecast(code);
         }
      }
   }
}
