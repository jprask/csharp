﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DatabaseApp {
    public partial class FrmAuthorsData : Form {
        private Authors author = new Authors();

        public FrmAuthorsData(string idAuthor) {
            InitializeComponent();
            author.AU_ID = idAuthor;

            GetAuthorData();
        }

        private void GetAuthorData() {
            if (author.AU_ID != string.Empty) {
                author.SearchForUpdate(author.AU_ID);
                tbFirstName.Text = author.AU_FNAME;
                tbLastName.Text = author.AU_LNAME;
                tbPhone.Text = author.PHONE;

                Titles t = new Titles();
                dgvTitles.DataSource = t.GetTitlesByAuthor(author.AU_ID);
            }
        }

        private void btnSave_Click(object sender, EventArgs e) {
            author.AU_FNAME = tbFirstName.Text;
            author.AU_LNAME = tbLastName.Text;
            author.PHONE = tbPhone.Text;
            if (author.AU_ID != string.Empty) {
                author.Update();
            }
            else {
                author.Insert();
            }
        }
    }
}
