﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccessManager.Enumerator {
   //Enumerators are a finite list of something
   //Sample 1: a list of colors => Blue, Red, Green, Yellow
   //Sample 2: a list of States => California, Florida, Texas, ...
   //For more information about it read C# Programming Yellow Book (Rob Milespag) pg 79
   public enum IpType { Ip4, Ip6}
}
