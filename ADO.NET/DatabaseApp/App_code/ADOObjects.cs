﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Xml;

namespace ADOObjects {
   public static class DB {
      private static string dataSource = "localhost";
      private static string database = "pubs";
      private static string user = "gest";
      private static string password = "gest@2018";
      private static SqlConnection connection;
      public static SqlConnection Connection { get { return Open(); } }
      //instancia e abre a conexao
      public static SqlConnection Open() {
         connection = new SqlConnection();
         string cs = "Data Source={0};Initial Catalog={1};User id={2};Password={3};";
         connection.ConnectionString = string.Format(cs, dataSource, database, user, password);
         try {
            connection.Open();
            return connection;
         }
         catch (Exception error) {
            throw error;
         }

      }//public static SqlConnection Open()

      //close connection
      public static void Close() {
         connection.Close();
      }
   }//public static class SQLServer 

   public class Authors {
      public Object GetNumberOfRows() {
         string sql = "select count(*) from authors";
         SqlCommand cmd = new SqlCommand(sql, DB.Connection);
         Object obj = cmd.ExecuteScalar();
         DB.Close();
         try {
            return obj;
         }
         catch (Exception error) {
            throw error;
         }
      }//public Object GetNumberOfRows() 


      public int SetState(string from, string to) {
         //pay attention: there is apostofre in this string
         string sql = string.Format("UPDATE authors SET state = '{0}' WHERE state = '{1}'", to, from);
         SqlCommand cmd = new SqlCommand(sql, DB.Connection);
         int i;
         try {
            i = cmd.ExecuteNonQuery();
         }
         catch (Exception error) {
            throw error;
         }
         finally {
            DB.Close();
         }
         return i;
      }//public Object GetNumberOfRows() 

      public SqlDataReader GetAuthors() {
         string sql = "SELECT au_fname, au_lname FROM authors ORDER BY au_fname";
         SqlCommand cmd = new SqlCommand(sql, DB.Connection);
         SqlDataReader rd;
         try {
            rd = cmd.ExecuteReader();
         }
         catch (Exception error) {
            throw error;
         }
         return rd;
      }//public SqlDataReader GetAuthors()

      public string GetAuthorsXML() {
         StringBuilder sql = new StringBuilder();
         sql.Append("SELECT au_fname, au_lname ");
         sql.Append("FROM authors ");
         sql.Append("ORDER BY au_fname ");
         sql.Append("FOR XML AUTO");
         //you can try others option RAW('?'), ROOT('?'), ELEMENTS");

         SqlCommand cmd = new SqlCommand(sql.ToString(), DB.Connection);

         StringBuilder sb = new StringBuilder();

         try {
            XmlReader xmlrd = cmd.ExecuteXmlReader();
            
               xmlrd.Read();

            while (xmlrd.ReadState != System.Xml.ReadState.EndOfFile) {
               sb.AppendLine(xmlrd.ReadOuterXml());
            }
            
            DB.Close();
         }
         catch (Exception error) {
            throw error;
         }
         return sb.ToString();
      }//public string GetAuthorsXML()

      public DataTable GetAuthors(string name) {
         string sql = "SELECT au_fname, au_lname, state FROM authors WHERE au_fname like '{0}%' ORDER BY au_fname";
         sql = string.Format(sql, name);
         SqlCommand cmd = new SqlCommand(sql, DB.Connection);
         SqlDataAdapter da = new SqlDataAdapter(cmd);

         //DataTable store the dataset resulting from SELECT (bidirectional)
         DataTable dt = new DataTable();
         try {
            //fill DataTable 
            da.Fill(dt);
         }
         catch (Exception error) {
            throw error;
         }
         finally {
            DB.Close();
         }
         return dt;
      }//public static DataTable Select(string sql)


   }
}
