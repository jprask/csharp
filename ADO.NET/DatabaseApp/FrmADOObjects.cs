﻿using System;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.Xml;

namespace DatabaseApp {
   public partial class FrmADOObjects : Form {
      public FrmADOObjects() {
         InitializeComponent();
      }

      private void btnConnection_Click(object sender, EventArgs e) {
         try {
            SqlConnection con = ADOObjects.DB.Open();
            lblConnect.Text = "Connection successful";
         }
         catch (Exception error) {
            lblConnect.Text = "Connection failed. Server message: " + error.Message;
         }
      }

      private void btnCommandExecuteScalar_Click(object sender, EventArgs e) {
         ADOObjects.Authors authors = new ADOObjects.Authors();
         lblNumerOfRows.Text = authors.GetNumberOfRows().ToString();
      }

      private void btnCommandExecuteReader_Click(object sender, EventArgs e) {
         ADOObjects.Authors authors = new ADOObjects.Authors();
         SqlDataReader dr = authors.GetAuthors();
         lbAuthors.Items.Clear();
         if (dr.HasRows) {
            while (dr.Read()) {
               string name = dr["au_fname"].ToString() + " " + dr["au_lname"].ToString();
               lbAuthors.Items.Add(name);
            }
         }
         ADOObjects.DB.Close();
      }

      private void btnCommandExecuteNonQuery_Click(object sender, EventArgs e) {
         ADOObjects.Authors authors = new ADOObjects.Authors();
         int i = authors.SetState(tbStateFrom.Text, tbStateTo.Text);
         lblNumerOfRowsUpdated.Text = i.ToString();
      }

      private void btnCommandExecuteXmlReader_Click(object sender, EventArgs e) {
         ADOObjects.Authors authors = new ADOObjects.Authors();
         tbXML.Text = authors.GetAuthorsXML();
      }

      private void btnSqlDataAdapter_Click(object sender, EventArgs e) {
         ADOObjects.Authors authors = new ADOObjects.Authors();
         dtGrdAuthors.DataSource = authors.GetAuthors(tbName.Text);
      }
   }
}
