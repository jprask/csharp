﻿using System;
using System.Diagnostics;
using System.Windows.Forms;

namespace DatabaseApp {
   public partial class FrmMain : Form {
      public FrmMain() {
         InitializeComponent();
      }

      private void lnkLblDatabase_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e) {
         Process.Start((sender as LinkLabel).Text);
      }

      private void mnItmADOObjects_Click(object sender, EventArgs e) {
         FrmADOObjects frm = new FrmADOObjects();
         frm.ShowDialog();
      }

      private void mnItmAuthors_Click(object sender, EventArgs e) {
         FrmAuthorsSearch frm = new FrmAuthorsSearch();
         frm.ShowDialog();
      }

      private void mnItmXML_Click(object sender, EventArgs e) {
         FrmXML frm = new FrmXML();
         frm.ShowDialog();
      }

      private void mnItmXMLFromURL_Click(object sender, EventArgs e) {
         FrmXMLFromURL frm = new FrmXMLFromURL();
         frm.ShowDialog();
      }
   }
}
