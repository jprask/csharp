﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccessManager {
   public class Access {
      public IP Ip;
      public DateTime DateTime { get; private set; }

      public Access(string ip) {
         Ip = new IP(ip);
         DateTime = DateTime.Now;
      }
   }
}
