﻿namespace DatabaseApp {
   partial class FrmXMLFromURL {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing) {
         if (disposing && (components != null)) {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent() {
         this.dgvCities = new System.Windows.Forms.DataGridView();
         this.btnLoad = new System.Windows.Forms.Button();
         this.dgvxmlWeatherForecast = new System.Windows.Forms.DataGridView();
         ((System.ComponentModel.ISupportInitialize)(this.dgvCities)).BeginInit();
         ((System.ComponentModel.ISupportInitialize)(this.dgvxmlWeatherForecast)).BeginInit();
         this.SuspendLayout();
         // 
         // dgvCities
         // 
         this.dgvCities.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
         this.dgvCities.Location = new System.Drawing.Point(12, 12);
         this.dgvCities.Name = "dgvCities";
         this.dgvCities.Size = new System.Drawing.Size(578, 211);
         this.dgvCities.TabIndex = 0;
         this.dgvCities.SelectionChanged += new System.EventHandler(this.dgvCities_SelectionChanged);
         // 
         // btnLoad
         // 
         this.btnLoad.Location = new System.Drawing.Point(596, 12);
         this.btnLoad.Name = "btnLoad";
         this.btnLoad.Size = new System.Drawing.Size(101, 35);
         this.btnLoad.TabIndex = 1;
         this.btnLoad.Text = "Load";
         this.btnLoad.UseVisualStyleBackColor = true;
         this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
         // 
         // dgvxmlWeatherForecast
         // 
         this.dgvxmlWeatherForecast.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
         this.dgvxmlWeatherForecast.Location = new System.Drawing.Point(13, 240);
         this.dgvxmlWeatherForecast.Name = "dgvxmlWeatherForecast";
         this.dgvxmlWeatherForecast.Size = new System.Drawing.Size(677, 249);
         this.dgvxmlWeatherForecast.TabIndex = 2;
         // 
         // FrmXMLFromURL
         // 
         this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.ClientSize = new System.Drawing.Size(702, 501);
         this.Controls.Add(this.dgvxmlWeatherForecast);
         this.Controls.Add(this.btnLoad);
         this.Controls.Add(this.dgvCities);
         this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
         this.Name = "FrmXMLFromURL";
         this.Text = "FrmXMLFromURL";
         ((System.ComponentModel.ISupportInitialize)(this.dgvCities)).EndInit();
         ((System.ComponentModel.ISupportInitialize)(this.dgvxmlWeatherForecast)).EndInit();
         this.ResumeLayout(false);

      }

      #endregion

      private System.Windows.Forms.DataGridView dgvCities;
      private System.Windows.Forms.Button btnLoad;
      private System.Windows.Forms.DataGridView dgvxmlWeatherForecast;
   }
}