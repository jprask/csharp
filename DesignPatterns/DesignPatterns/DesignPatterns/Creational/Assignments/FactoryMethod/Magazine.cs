﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Creational.Assignments.FactoryMethod {
    public enum Subject {
        Adult,
        Sports,
        Fashion
    }

    abstract class Magazine {
        public abstract Subject Subject { get; }
        public abstract int Pages { get; }
        public abstract string Title { get; }
        public abstract Double Price { get; }
    }
}
