﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Creational.Assignments.FactoryMethod {
    class PlayboyFactory : MagazineFactory {

        private int pages;
        private string title;

        public PlayboyFactory(int pages, string title) {
            this.pages = pages;
            this.title = title;
        }

        public Magazine getMagazine() {
            return new Playboy(pages, title);
        }
    }
}
