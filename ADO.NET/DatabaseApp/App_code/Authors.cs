﻿using System.Data;
using System.Linq;
using System.Text;
using SQLServer;
using System.Data.SqlClient;

namespace DatabaseApp {
    public class Authors : AUTHORS {
        public int Insert() {
            StringBuilder sb = new StringBuilder();
            sb.Append("INSERT INTO authors (au_fname) ");
            sb.AppendFormat("VALUES ('{0}') ", this.AU_FNAME);
            return SQL.ExecuteNonQuery(sb.ToString());
        }//public int Insert()

        public int Update() {
            StringBuilder sb = new StringBuilder();
            sb.Append("UPDATE authors ");
            sb.AppendFormat("SET au_fname = '{0}' ", this.AU_FNAME);
            //sb.AppendFormat("SET au_fname = @au_fname ");
            sb.AppendFormat("WHERE au_id = '{0}'", this.AU_ID);
            return SQL.ExecuteNonQuery(sb.ToString());
            //You MUST use params
        }//public int Insert()

        public void SearchForUpdate(string idAuthor) {
            string sql = string.Format("SELECT * FROM authors WHERE au_id = '{0}'", idAuthor);
            //option 1 using DataTable
            //DataTable dt = SQL.Select(sql);
            //this.AU_ID = dt.Rows[0]["au_id"].ToString();
            // ...
            //option 2 using SqlDataReader
            SqlDataReader dr = SQL.ExecuteReader(sql);
            dr.Read();
            this.AU_ID = dr["au_id"].ToString();
            this.AU_FNAME = dr["au_fname"].ToString();
            this.AU_LNAME = dr["au_lname"].ToString();
            this.PHONE = dr["phone"].ToString();
            DB.Close();
            //read other fields here
        }//public void SearchForUpdate(int idAuthor)

        public DataTable Search(string name) {
            StringBuilder sb = new StringBuilder();
            sb.Append("SELECT au_id, au_fname, au_lname, phone ");
            sb.Append("FROM authors ");
            if (name.Trim() != string.Empty) {
                sb.AppendFormat("WHERE au_fname like '%{0}%' ", name);
            }
            sb.Append("ORDER BY au_fname");

            return SQL.Select(sb.ToString());
        }//public DataTable Search(string name) 
    }
}
