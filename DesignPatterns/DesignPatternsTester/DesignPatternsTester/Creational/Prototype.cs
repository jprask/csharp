﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DesignPatterns.Creational.Prototype;

namespace DesignPatternsTester.Creational {
   public class Prototype {
      //original example
      //http://www.blackwasp.co.uk/Prototype.aspx

      public void Run() {
         Developer dev = new Developer();
         dev.Name = "Bob";
         dev.Role = "Team Leader";
         dev.PreferredLanguage = "C#";

         Developer devCopy = (Developer)dev.Clone();
         devCopy.Name = "Sue";

         /** try to do this:
         Developer devCopy = new Developer();
         devCopy = dev;
         devCopy.Name = "Sue";
         */

         Console.WriteLine(dev);
         Console.WriteLine(devCopy);

         Console.WriteLine("");

         Typist typist = new Typist();
         typist.Name = "Kay";
         typist.Role = "Typist";
         typist.WordsPerMinute = 120;

         Typist typistCopy = (Typist)typist.Clone();
         typistCopy.Name = "Tim";
         typistCopy.WordsPerMinute = 115;

         Console.WriteLine(typist);
         Console.WriteLine(typistCopy);

      }
   }
}
