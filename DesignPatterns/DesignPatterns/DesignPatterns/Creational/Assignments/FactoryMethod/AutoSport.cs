﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Creational.Assignments.FactoryMethod {
    class AutoSport : Magazine {
        private Subject subject = Subject.Sports;
        private int pages;
        private string title;
        private double price;

        public AutoSport(int pages, string title) {
            this.pages = pages;
            this.title = title;
            this.price = 5 * pages;
        }

        public override Subject Subject => subject;

        public override int Pages => pages;

        public override string Title => title;

        public override double Price => price;
    }
}
