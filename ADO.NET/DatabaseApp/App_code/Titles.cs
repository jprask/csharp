﻿using SQLServer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseApp {
    public class Titles {
        public DataTable GetTitlesByAuthor(string au_id) {
            StringBuilder sb = new StringBuilder();
            sb.Append("SELECT t.title, t.price ");
            sb.Append("FROM titleauthor ta ");
            sb.Append("INNER JOIN titles t on t.title_id = ta.title_id ");
            sb.AppendFormat("WHERE au_id = '{0}'", au_id);
            sb.Append("ORDER BY t.title");
            return SQL.Select(sb.ToString());
        }
    }
}
