﻿namespace DesignPatterns.Creational.Assignments.Builder {
    public enum ModelType {
        iPhoneX,
        Redmi
    }

    public class Model {
        private int Year { get; }
        private ModelType Type { get; }
        
        public Model(int y, ModelType t) {
            this.Year = y;
            this.Type = t;
        }
    }
}