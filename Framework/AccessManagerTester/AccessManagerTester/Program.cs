﻿using AccessManager;
using System;


namespace AccessManagerTester {
   class Program {
      static void Main(string[] args) {
         AccessLog log = new AccessLog();
         log.Add("1.1.1.1");
         log.Add("1.1.1.2");
         log.Add("1.1.1.3");
         log.Add("1.1.1.1");
         log.Add("1.1.1.1");
         log.Add("1.1.1.2");
         log.Add("1.1.1.4");
         foreach (Access a in log.All()) {
            Console.WriteLine("IP: {0} - {1}", a.Ip.Number, a.DateTime);
         }

         Access access = log.FirstAccess();
         Console.WriteLine("First Access: {0} - {1}", access.Ip.Number, access.DateTime);

         access = log.LastAccess();
         Console.WriteLine("Last Access: {0} - {1}", access.Ip.Number, access.DateTime);

         Console.WriteLine("Number of access: {0}", log.Count());

         Console.ReadKey();
      }
   }
}
