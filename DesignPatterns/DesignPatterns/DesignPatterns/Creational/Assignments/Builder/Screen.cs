﻿namespace DesignPatterns.Creational.Assignments.Builder {
    public class Screen {
        private int width { get; set; }
        private int height { get; set; }

        public Screen(int w, int h) {
            this.width = w;
            this.height = h;
        }
    }
}