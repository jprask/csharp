﻿using System;
using System.Data;
using System.Data.OleDb;

namespace OleDB {
    public static class DB {
        private static OleDbConnection connection;
        public static OleDbConnection Connection { get { return Open(); } }
        //instancia e abre a conexao
        public static OleDbConnection Open() {
            if (connection == null) {
                connection = new OleDbConnection();
                connection.ConnectionString = "PROVIDER=Microsoft.Jet.OLEDB.4.0;" +
                                               @"Data Source=d:\myDatabase.mdb;";
                try {
                    connection.Open();
                    return connection;
                }
                catch (Exception error) {
                    throw error;
                }
            }
            else {
                return connection;
            }
        }//public static OleDbConnection Open()

        //close connection
        public static void Close() {
            connection.Close();
        }
    }//public static class OleDB 

    public static class SQL {
        public static DataTable Select(string sql) {
            OleDbCommand cmd = new OleDbCommand(sql, DB.Connection);
            OleDbDataAdapter da = new OleDbDataAdapter(cmd);
            //DataTable store the dataset resulting from SELECT (bidirectional)
            DataTable dt = new DataTable();
            try {
                //fill DataTable 
                da.Fill(dt);
            }
            catch (Exception error) {
                throw error;
            }
            finally {
                DB.Close();
            }
            return dt;
        }//public static DataTable Select(string sql)

        public static OleDbDataReader ExecuteReader(string sql) {
            OleDbCommand cmd = new OleDbCommand(sql, DB.Connection);
            OleDbDataReader rd;
            try {
                rd = cmd.ExecuteReader();
            }
            catch (Exception error) {
                throw error;
            }
            return rd;
        }//public static OleDbDataReaderr ExecuteReader(string sql) 

        public static Object ExecuteScalar(string sql) {
            OleDbCommand cmd = new OleDbCommand(sql, DB.Connection);
            Object obj;
            try {
                obj = cmd.ExecuteScalar();
            }
            catch (Exception error) {
                throw error;
            }
            finally {
                DB.Close();
            }
            return obj;
        }//public static Object ExecuteScalar(string sql)


        public static int ExecuteNonQuery(string sql) {
            //ExecuteNonQuery returns number of rows affected (ex: 2 rows updated)
            OleDbCommand cmd = new OleDbCommand(sql, DB.Connection);
            int i;
            try {
                i = cmd.ExecuteNonQuery();
            }
            catch (Exception error) {
                throw error;
            }
            finally {
                DB.Close();
            }
            return i;
        }//public static int ExecuteNonQuery(string sql)

    } //public static class SQL
}//namespace OleDB
