﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseApp {
   public class AUTHORS {
      public string AU_ID { get; set; }
      public string AU_LNAME { get; set; }
      public string AU_FNAME { get; set; }
      public string PHONE { get; set; }
      public string ADDRESS { get; set; }
      public string CITY { get; set; }
      public string STATE { get; set; }
      public string ZIP { get; set; }
      public bool CONTRACT { get; set; }
   }
}
