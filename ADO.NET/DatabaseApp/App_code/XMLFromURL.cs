﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace DatabaseApp {
   public class City {
      public string Code { get; set; }
      public string State { get; set; }
      public string Name { get; set; }
   }

   public class Prevision {
      public string date { get; set; }
      public string weather { get; set; }
      public string max { get; set; }
      public string min { get; set; }
      public string iuv { get; set; }
   }

   public class XMLFromURL {
      public List<City> Cities = new List<City>();
      
      public void LoadCities() {
         var url = "http://servicos.cptec.inpe.br/XML/listaCidades";
         string xmlStr = new WebClient().DownloadString(url);
         var xmlDoc = new XmlDocument();
         xmlDoc.LoadXml(xmlStr);

         XmlNodeList myNodeList = xmlDoc.SelectNodes("//cidades/cidade");
         foreach (XmlNode node in myNodeList) {
            City c = new City() {
               Code = node.SelectSingleNode("id").InnerText,
               State = node.SelectSingleNode("uf").InnerText,
               Name = node.SelectSingleNode("nome").InnerText
            };
            Cities.Add(c);
         }
      }

      public List<Prevision> WeatherForecast(string code) {
         List<Prevision> list = new List<Prevision>();
         var url = string.Format("http://servicos.cptec.inpe.br/XML/cidade/{0}/previsao.xml", code);
         string xmlStr = new WebClient().DownloadString(url);
         var xmlDoc = new XmlDocument();
         xmlDoc.LoadXml(xmlStr);

         XmlNodeList myNodeList = xmlDoc.SelectNodes("//cidade/previsao");
         foreach (XmlNode node in myNodeList) {
            Prevision p = new Prevision() {
               date = node.SelectSingleNode("dia").InnerText,
               weather = node.SelectSingleNode("tempo").InnerText,
               max = node.SelectSingleNode("maxima").InnerText,
               min = node.SelectSingleNode("minima").InnerText,
               iuv = node.SelectSingleNode("iuv").InnerText
            };
            list.Add(p);
         }
         return list;
      }
   }
}

